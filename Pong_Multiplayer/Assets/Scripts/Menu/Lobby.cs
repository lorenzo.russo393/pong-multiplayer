using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lobby : MonoBehaviour 
{
    [SerializeField] GameObject lobby;

    [SerializeField] TMP_InputField addressText;

    [SerializeField] TextMeshProUGUI player_name;

    [SerializeField] private TMP_Text[] playerNameTexts = new TMP_Text[0];

    [SerializeField] private Button startButton;

    string address;

    private void OnEnable()
    {
        NetworkManager_Pong.OnClientConnected += ClientConnected;
        NetworkManager_Pong.OnClientDisconnected += ClientDisconnected;
        NetworkPlayer.ClientOnPlayerInfoUpdated += ClientHandlePlayerInfo;
    }

    private void OnDisable()
    {
        NetworkManager_Pong.OnClientConnected -= ClientConnected;
        NetworkManager_Pong.OnClientDisconnected -= ClientDisconnected;
        NetworkPlayer.ClientOnPlayerInfoUpdated -= ClientHandlePlayerInfo;
    }

    public void HostLobby()
    {
        lobby.SetActive(true);
        NetworkManager_Pong.singleton.StartHost();
    }

    public void ConfirmAddress()
    {
        address = addressText.text;
        if (address == "") return;

        NetworkManager_Pong.singleton.networkAddress = address;
        NetworkManager_Pong.singleton.StartClient();
        lobby.SetActive(true);
    }

    public void LeaveLobby()
    {
        if(NetworkServer.active && NetworkClient.isConnected)
        {
            NetworkManager_Pong.singleton.StopHost();
        }
        else
        {
            NetworkManager_Pong.singleton.StopClient();
            SceneManager.LoadScene(0);
        }
    }

    private void ClientConnected()
    {

    }

    private void ClientDisconnected()
    {

    }


    private void ClientHandlePlayerInfo()
    {
        List<NetworkPlayer> players = ((NetworkManager_Pong)NetworkManager.singleton).players;

        for(int i = 0; i < players.Count; i++)
        {
            playerNameTexts[i].text = players[i].PlayerName;
        }

        for (int i = players.Count; i < playerNameTexts.Length; i++)
        {
            playerNameTexts[i].text = "Waiting for player..";
        }

        //startButton.interactable = players.Count > 1;
    }
}
