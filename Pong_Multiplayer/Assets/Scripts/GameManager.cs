using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
using Mirror.Examples.Pong;
using System;
using UnityEngine.SceneManagement;

public class GameManager : NetworkBehaviour
{
    public static Action OnScored;

    [SerializeField] int scoreMax = 5;

    [SyncVar(hook = nameof(UpdateHostScore))]
    private int hostScore;

    [SyncVar(hook = nameof(UpdateGuestScore))]
    private int guestScore;

    [SerializeField] GameObject hostNet;
    [SerializeField] GameObject guestNet;

    [SerializeField] private TextMeshProUGUI hostScore_text;
    [SerializeField] private TextMeshProUGUI guestScore_text;

    [SerializeField] GameObject winUI;
    [SerializeField] TextMeshProUGUI winner_text;

    private void OnEnable()
    {
        Ball.OnGoal += CheckGoal;
    }

    private void OnDisable()
    {
        Ball.OnGoal -= CheckGoal;
    }

    public override void OnStartServer()
    {
        hostScore = 0;
        guestScore = 0;

        WinCondition();
    }

    [ServerCallback]
    public void WinCondition()
    {  
        if(hostScore >= scoreMax || guestScore >= scoreMax)
        {
            NetworkManager_Pong.singleton.StopClient();
            NetworkManager_Pong.singleton.StopHost();
            SceneManager.LoadScene(0);
        }
    }

    [ServerCallback]
    public void CheckGoal(GameObject net)
    {
        OnScored?.Invoke();

        if(net == guestNet)
        {
            hostScore++;
            WinCondition();
        }
        else if(net == hostNet)
        {
            guestScore++;
            WinCondition();
        }
    }

    public void UpdateHostScore(int oldScore, int newScore)
    {
        hostScore = newScore;
        hostScore_text.text = hostScore.ToString();
    }
    public void UpdateGuestScore(int oldScore, int newScore)
    {
        guestScore = newScore;
        guestScore_text.text = guestScore.ToString();
    }
}
