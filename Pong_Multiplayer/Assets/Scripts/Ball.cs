using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public static System.Action<GameObject> OnGoal;

    private Rigidbody2D rb;
    Vector2 direction;

    [SerializeField] float move_speed = 2;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        direction = Vector2.one.normalized;
        direction = new Vector2(Random.Range(0.2f, 1), Random.Range(0.2f, 1));
    }

    private void FixedUpdate()
    {
        rb.velocity = direction * move_speed;
    }

    private void OnEnable()
    {
        GameManager.OnScored += ResetPos;
    }

    private void OnDisable()
    {
        GameManager.OnScored -= ResetPos;
    }

    private IEnumerator Launch()
    {
        yield return new WaitForSeconds(2f);

        rb.bodyType = RigidbodyType2D.Dynamic;
        direction = Vector2.one.normalized;
        direction = new Vector2(Random.Range(0.2f, 1), Random.Range(0.2f, 1));
    }

    [ServerCallback]
    public void ResetPos()
    {
        transform.position = Vector2.zero;
        rb.velocity = Vector2.zero;
        direction = Vector2.zero;
        rb.bodyType = RigidbodyType2D.Kinematic;

        StartCoroutine(Launch());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Wall"))
        {
            direction.y = -direction.y;
        }
        else if(collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("HorizontalWall"))
        {
            direction.x = -direction.x;
        }
        else if(collision.gameObject.CompareTag("Goal"))
        {
            OnGoal?.Invoke(collision.gameObject);
        }

        if(collision.gameObject.CompareTag("Player"))
        {
            move_speed += 0.5f;
        }
    }
}
