using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI hostName;
    [SerializeField] TextMeshProUGUI guestName;

    [SerializeField] TextMeshProUGUI hostScore;
    [SerializeField] TextMeshProUGUI guestScore;

    private void OnEnable()
    {
        NetworkManager_Pong.OnSceneChanged += ChangeUIName;
    }
    private void OnDisable()
    {
        NetworkManager_Pong.OnSceneChanged -= ChangeUIName;
    }

    private void ChangeUIName(string name, NetworkPlayer player)
    {
        if (player.IsHost)
            hostName.text = name;
        else
            guestName.text = name;
    }
}
