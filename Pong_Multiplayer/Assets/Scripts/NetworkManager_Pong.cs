using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager_Pong : NetworkManager
{
    public List<NetworkPlayer> players = new List<NetworkPlayer>();

    public static Action OnClientConnected;
    public static Action OnClientDisconnected;
    public static Action<string, NetworkPlayer> OnSceneChanged;

    GameObject ball;

    public bool gameStarted = false;

    [SerializeField] GameObject player_prefab;

    #region Server

    /*public override void OnServerChangeScene(string newSceneName)
    {
        print(SceneManager.GetActiveScene().name);

        if (newSceneName == "MenuLobby")
        {
            foreach(Player p in players)
            {
                print("Sesso forte");

                if (leftSpawn == null || rightSpawn == null) return;

                Transform positionToSpawn;

                if (numPlayers == 0)
                {
                    positionToSpawn = leftSpawn.transform;
                }
                else
                {
                    positionToSpawn = rightSpawn.transform;
                }
               
                GameObject obj = Instantiate(playerPrefab, p.posToSpawn, p.transform.rotation);
                NetworkServer.Spawn(obj, p.connectionToClient);
                //NetworkServer.AddPlayerForConnection(conn, obj);

                if (numPlayers > 0)
                {
                    ball = Instantiate(spawnPrefabs.Find(prefab => prefab.name == "Ball"));
                    NetworkServer.Spawn(ball);
                }
            }
        }
    }
    */

    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        base.OnServerAddPlayer(conn);

        NetworkPlayer network_player = conn.identity.GetComponent<NetworkPlayer>();
        //Player player = conn.identity.GetComponent<Player>();
        network_player.netIdentity.AssignClientAuthority(conn);
        players.Add(network_player);   

        network_player.SetName($"Player {players.Count}");

        network_player.SetIsHost(players.Count == 1);

        print(network_player.IsHost);

        /*if(player.IsHost)
        {
            Instantiate(player, leftSpawn.transform.position, Quaternion.identity);
        }
        else
        {
            Instantiate(player, rightSpawn.transform.position, Quaternion.identity);

        }
        */
    }

    /*public override void OnServerConnect(NetworkConnectionToClient conn)
    {
        if (!gameStarted) return;

        conn.Disconnect();
        base.OnServerConnect(conn);
    }
    */

    public void QuitLobby()
    {
        if (NetworkServer.active && NetworkClient.isConnected)
        {
            StopHost();
            StopClient();
        }
        else
        {
            StopClient();
        }
    }

    public override void OnServerConnect(NetworkConnectionToClient conn)
    {
        if (!gameStarted) return;

        conn.Disconnect();

        base.OnServerConnect(conn);
    }

    public override void OnServerDisconnect(NetworkConnectionToClient conn)
    {
        //base.OnServerDisconnect(conn);

        NetworkPlayer network_player = conn.identity.GetComponent<NetworkPlayer>();

        if (players.Count == 2 && players[1] == network_player)
            QuitLobby();

        players.Remove(network_player);

        base.OnServerDisconnect(conn);
    }

    public override void OnStopServer()
    {
        players.Clear();
        gameStarted = false;      

        //base.OnStopServer();
    }

    public void StartGame()
    {
        if (players.Count < 2) return;       

        gameStarted = true;

        ServerChangeScene("MainScene");
    }

    public override void OnServerSceneChanged(string sceneName)
    {
        base.OnServerSceneChanged(sceneName);

        print(SceneManager.GetActiveScene().name);

        if(SceneManager.GetActiveScene().name.StartsWith("Main"))
        {
            foreach(NetworkPlayer p in players)
            {
                GameObject playerObj = Instantiate(player_prefab, GetStartPosition().position, Quaternion.identity);
                NetworkServer.ReplacePlayerForConnection(p.connectionToClient, playerObj);
                NetworkServer.Spawn(playerObj, p.connectionToClient);
                OnSceneChanged?.Invoke(p.PlayerName, p);
            }          
            ball = Instantiate(spawnPrefabs.Find(prefab => prefab.name == "Ball"));
            NetworkServer.Spawn(ball);
        }
    }
    #endregion

    #region Client

    public override void OnStopClient()
    {
        base.OnStopClient();
        players.Clear();
        gameStarted = false;
    }

    public override void OnClientConnect()
    {
        base.OnClientConnect();

        OnClientConnected?.Invoke();
    }

    public override void OnClientDisconnect()
    {
        base.OnClientDisconnect();

        OnClientDisconnected?.Invoke();

        gameStarted = false;
    }

    public override void OnClientSceneChanged()
    {
        NetworkClient.Ready();
        base.OnClientSceneChanged();
    }
    #endregion
}
