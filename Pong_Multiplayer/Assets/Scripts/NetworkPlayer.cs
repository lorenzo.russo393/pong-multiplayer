using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using System.Runtime.CompilerServices;
using TMPro;

public class NetworkPlayer : NetworkBehaviour
{
    public static Action ClientOnPlayerInfoUpdated;
    public static event Action<bool> AuthorityOnPartyOwnerUpdated;

    [SyncVar(hook = nameof(AuthorityHandlePartyOwnerUpdated))]
    private bool isHost = false;

    [SyncVar(hook = nameof(ClientHandlePlayerNameUpdated))]
    private string playerName;

    public string PlayerName { get { return playerName; } }
    public bool IsHost { get { return isHost; } }   

    public override void OnStartServer()
    {
        DontDestroyOnLoad(gameObject);
    }

    public override void OnStartClient()
    {
        if (NetworkServer.active) return;

        ((NetworkManager_Pong)NetworkManager.singleton).players.Add(this);

        DontDestroyOnLoad(gameObject);
    }

    public override void OnStopClient()
    {
        if (!isClientOnly) return;

        ClientOnPlayerInfoUpdated?.Invoke();
        
        ((NetworkManager_Pong)NetworkManager.singleton).players.Remove(this);

        if (!isOwned) return;
    }

    [Command]
    public void CmdStartGame()
    {
        if (!isHost) return;

        ((NetworkManager_Pong)NetworkManager.singleton).StartGame();
    }

    [Server]
    public void SetName(string newName)
    {
        playerName = newName;
    }

    [Server]
    public void SetIsHost(bool v)
    {
        isHost = v;
    }

    public override void OnStartAuthority()
    {
        if (((NetworkManager_Pong)NetworkManager.singleton).gameStarted) return;
    }

    private void AuthorityHandlePartyOwnerUpdated(bool oldState, bool newState)
    {
        if (!isOwned) return;

        AuthorityOnPartyOwnerUpdated?.Invoke(newState);
    }

    private void ClientHandlePlayerNameUpdated(string oldName, string newName)
    {
        ClientOnPlayerInfoUpdated?.Invoke();
    }

    private void Update()
    {
        if (!isLocalPlayer) return;
    }
}
