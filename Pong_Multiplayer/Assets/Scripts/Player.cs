using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

public class Player : NetworkBehaviour
{


    private Rigidbody2D rb;

    [SerializeField] float move_speed = 1;

    private Vector2 velocity = Vector2.zero;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Movement()
    {
        if (!isLocalPlayer) return;

        Vector2 targetVelocity = new Vector2(0, Input.GetAxisRaw("Vertical") * move_speed);
        rb.velocity = Vector2.SmoothDamp(rb.velocity, targetVelocity, ref velocity, 0);
    }

    /*private void GetPlayerColor()
    {
        if (!isLocalPlayer) return;

        transform.GetChild(0).transform.GetComponent<SpriteRenderer>().color = Color.white;
    }
    */

    [ClientCallback]
    private void FixedUpdate()
    {
        Movement();
        //GetPlayerColor();
    }

   

    

    
    

    

   

    
}
